// khai báo thư viện express js
const express = require("express");

//khai báo router app
const router = express.Router();
//Import drink controller
const  courseController = require("../controller/courseController");

router.post("/courses", courseController.createCourse);

router.get("/courses", courseController.getAllCourse);

router.get("/courses/:courseId", courseController.getCourseById);

router.put("/courses", courseController.updateCourseByCourseCode);

router.delete("/courses", courseController.deleteCourseByCourseCode);

module.exports = router;