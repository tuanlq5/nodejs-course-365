//import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose 
const Schema  = mongoose.Schema;

//khởi tạo instance course Schema từ class Schema 
const courseSchema = new Schema({
    courseCode:{
        type: String,
        require: true,
        unique: true,
    },
    courseName: {
        type: String,
        require: true,
    },
    price:{
        type: Number,
        require:true,
    },
    discountPrice:{
        type: Number,
        require: true,
    },
    duration:{
        type: String,
        require: true,
    },
    level:{
        type: String,
        require: true,
    },
    coverImage:{
        type: String,
        require: true,
    },
    teacherName:{
        type: String,
        require: true,
    },
    teacherPhoto:{
        type: String,
        require: true,
    },
    isPopular:{
        type: Boolean,
        default: true,
    },
    isTrending:{
        type: Boolean,
        default: false,
    },
},{ versionKey: false }, {
    timestamps: true
})
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Course", courseSchema );