//import thư viện  mongoose
const {response, request} = require("express");
const mongoose = require("mongoose");

//import Model course vào file 
const courseModel = require("../models/courseModel");

const createCourse = (request, response) =>{
    //Bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // // {
    //     'bodyCourseCode': "ABC123",
    //     'bodyCourseName': "JS",
    //     'bodyPrice': 300,
    //     'bodyDiscountPrice': 10,
    //     'bodyDuration': "100h",
    //     'bodyLevel': "Beginner",
    //     'bodyCoverImage': "https://cdn.dribbble.com/userupload/3158902/file/original-7c71bfa677e61dea61bc2acd59158d32.jpg?resize=400x0",
    //     'bodyTeacherName': "Avalan"
    //     'bodyTeacherPhoto': "https://www.freepnglogos.com/uploads/teacher-png/teacher-make-footballers-franchising-40.png",
    //     'bodyIsPopular': false,
    //     'bodyIsTrending': true ,
    // // }
    //Bước 2:validate dữ liệu
    if(!body.courseCode){
        return response.status(400).json({
            status: "Bad Request",
            message: "Course code không hợp lệ"
        })
    }   
    if(!body.courseName){
        return response.status(400).json({
            status: "Bad Request",
            message: "Course name không hợp lệ"
        })
    }   
    if((isNaN(body.price) || body.price < 0) && !body.price){
        return response.status(400).json({
            status: "Bad Request",
            message: "Price không hợp lệ"
        })
    }   
    if((isNaN(body.discountPrice) || body.discountPrice < 0) && !body.discountPrice){
        return response.status(400).json({
            status: "Bad Request",
            message: "Discount price không hợp lệ"
        })
    }   
    if(!body.duration){
        return response.status(400).json({
            status: "Bad Request",
            message: "Duration không hợp lệ"
        })
    }   
    if(!body.level){
        return response.status(400).json({
            status: "Bad Request",
            message: "Level không hợp lệ"
        })
    }   
    if(!body.coverImage){
        return response.status(400).json({
            status: "Bad Request",
            message: "CoverImage không hợp lệ"
        })
    }   
    if(!body.teacherName){
        return response.status(400).json({
            status: "Bad Request",
            message: "TeacherName không hợp lệ"
        })
    }   
    if(!body.teacherPhoto){
        return response.status(400).json({
            status: "Bad Request",
            message: "TeacherPhoto không hợp lệ"
        })
    } 
    //bước 3: gọi Model tạo dữ liệu
    const newCourse = {
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
}  
    //bước 4: Trả về kết quả 
    courseModel.create(newCourse,(error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return response.status(201).json({
            status: "Create Course: Successfull",
            data:  data
        })
    });  
}
const getAllCourse = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    // bước 4: Trả về kết quả
    courseModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher: Successfull",
            data: data
        })
    })
}
const getCourseById = (request,response) =>{
    // bước 1: chuẩn bị dữ liệu
   const courseId  =  request.params.courseId;
   console.log(courseId);
   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(courseId)){
    return response.status(400).json({
            status: "Bad request",
            message: "Drink không hợp lệ" 
        })
    }
    //bước 3 - 4: gọi model tạo dữ liêu  và trả về 
    courseModel.findById(courseId,(error,data) =>{
        return response.status(200).json({
            status: "Get Detail Drink successfull",
            data: data
            })
        }
    )  
}
const updateCourseByCourseCode = (request, response) =>{
    //Bước 1: chuẩn bị dữ liệu
    const courseCode = request.query.courseCode;
    console.log(courseCode);
    const body = request.body;
    // // {
    //     'bodyCourseCode': "ABC123",
    //     'bodyCourseName': "JS",
    //     'bodyPrice': 300,
    //     'bodyDiscountPrice': 10,
    //     'bodyDuration': "100h",
    //     'bodyLevel': "Beginner",
    //     'bodyCoverImage': "https://cdn.dribbble.com/userupload/3158902/file/original-7c71bfa677e61dea61bc2acd59158d32.jpg?resize=400x0",
    //     'bodyTeacherName': "Avalan"
    //     'bodyTeacherPhoto': "https://www.freepnglogos.com/uploads/teacher-png/teacher-make-footballers-franchising-40.png",
    //     'bodyIsPopular': false,
    //     'bodyIsTrending': true ,
    // // }
    //Bước 2:validate dữ liệu
    if(!body.courseCode){
        return response.status(400).json({
            status: "Bad Request",
            message: "Course code không hợp lệ"
        })
    }   
    if(!body.courseName){
        return response.status(400).json({
            status: "Bad Request",
            message: "Course name không hợp lệ"
        })
    }   
    if((isNaN(body.price) || body.price < 0) && !body.price){
        return response.status(400).json({
            status: "Bad Request",
            message: "Price không hợp lệ"
        })
    }   
    if((isNaN(body.discountPrice) || body.discountPrice < 0) && !body.discountPrice){
        return response.status(400).json({
            status: "Bad Request",
            message: "Discount price không hợp lệ"
        })
    }   
    if(!body.duration){
        return response.status(400).json({
            status: "Bad Request",
            message: "Duration không hợp lệ"
        })
    }   
    if(!body.level){
        return response.status(400).json({
            status: "Bad Request",
            message: "Level không hợp lệ"
        })
    }   
    if(!body.coverImage){
        return response.status(400).json({
            status: "Bad Request",
            message: "CoverImage không hợp lệ"
        })
    }   
    if(!body.teacherName){
        return response.status(400).json({
            status: "Bad Request",
            message: "TeacherName không hợp lệ"
        })
    }   
    if(!body.teacherPhoto){
        return response.status(400).json({
            status: "Bad Request",
            message: "TeacherPhoto không hợp lệ"
        })
    } 
    //bước 3: gọi Model tạo dữ liệu
    const newCourse = {
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
} 
    //bước 4: Trả về kết quả 
    courseModel.findOneAndUpdate({courseCode: courseCode},newCourse,{new:true},(error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return response.status(201).json({
            status: "Update Course: Successfull",
            data:  data
        })
    });  
}
const deleteCourseByCourseCode = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const courseCode = request.query.courseCode;

    //B2: Validate dữ liệu
    //B3:  Gọi model tạo dữ liệu
    courseModel.findOneAndDelete({courseCode: courseCode}, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Delete course successfully"
        })
    })
}
module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseByCourseCode,
    deleteCourseByCourseCode
}
    