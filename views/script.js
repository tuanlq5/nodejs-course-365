        import $ from "jquery"
        /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        var gCoursesDB = {
            description: "This DB includes all courses in system",
            courses: [
            ]
        }
        /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        $(document).ready(function (){
            onPageLoading();
        })
        /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
            // hàm xử lý sự kiện khi tải trang
            function onPageLoading(){
                "use strict";
                // bước 1: đọc gán dữ liệu
                // bước 2: kiểm tra dữ liệu (không cần)
                // bước 3: request API lấy data
                $.ajax({
                    type: "GET",
                    async: false,
                    url: "/courses",
                    success: function(data){
                       gCoursesDB.courses = data.data;
                       console.log(gCoursesDB);
                    }
                })
                console.log(gCoursesDB);
                var vArrayCoursePopular = findCourseIsPopular();
                var vArrayCourseTrending = findCourseIsTrending();
                // bước 4: xử lý hiển thị
                hienThiCoursePopular(vArrayCoursePopular);
                hienThiCourseTrending(vArrayCourseTrending);
            }
        /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/     
            // hàm xử tìm kiểm course popular 
            function findCourseIsPopular(){
                "use strict";
                var vObject = [];
                for(var bI = 0; bI < gCoursesDB.courses.length; bI++){
                    if(gCoursesDB.courses[bI].isPopular === true){
                        vObject.push(gCoursesDB.courses[bI]);
                    }
                }
                return vObject;
            }
            // hàm xử lý hiển thị trên popular courses
            function hienThiCoursePopular(paramObject){
                "use strict";
                $(".card-courses-popular").empty();
                var vMapCoursesPopupar = paramObject.map(function(paramCourses){
                    $(".card-courses-popular")
                        .append($("<div>").addClass("col-sm-3 mb-3")
                            .append($("<div>").addClass("card").css("width","16rem")
                                .append($("<img>").addClass("card-img-top").attr("src",paramCourses.coverImage))
                                .append($("<div>").addClass("card-body")
                                    .append($("<a>").addClass("font-weight-normal").html(paramCourses.courseName))
                                    .append($("<div>").addClass("d-flex  mt-4 mb-3")
                                        .append($("<i>").addClass("fa-regular fa-clock mt-1"))
                                        .append($("<p>").addClass("card-text").html("&ensp;"+ paramCourses.duration +"&ensp;" + paramCourses.level)))
                                    .append($("<p>").addClass("font-weight-bold").html("$"+paramCourses.discountPrice + "&emsp;")
                                        .append($("<del>").addClass("font-weight-light text-muted text-muted").html("$" + paramCourses.price))
                                )
                        )
                        .append($("<ul>").addClass("list-group list-group-flush")
                            .append($("<li>").addClass("list-group-item d-flex justify-content-between  align-items-center")
                                .append($("<img>").addClass("avatar-img").attr("src",paramCourses.teacherPhoto))
                                .append($("<p>").addClass("mb-2").html("&ensp;" + paramCourses.teacherName ))
                                .append($("<i>").addClass("fa-regular fa-bookmark ")))
                                )
                            )
                        )
                        
                }).join("");
            }
            // ham xử lý hiển thị trending courses
            function hienThiCourseTrending(paramObject){
                "use strict";
                $(".card-courses-trending").empty();
                var vMapCoursesPopupar = paramObject.map(function(paramCourses){
                    $(".card-courses-trending")
                        .append($("<div>").addClass("col-sm-3 mb-3")
                            .append($("<div>").addClass("card").css("width","16rem")
                                .append($("<img>").addClass("card-img-top").attr("src",paramCourses.coverImage))
                                .append($("<div>").addClass("card-body")
                                    .append($("<a>").addClass("font-weight-normal").html(paramCourses.courseName))
                                    .append($("<div>").addClass("d-flex  mt-4 mb-3")
                                        .append($("<i>").addClass("fa-regular fa-clock mt-1"))
                                        .append($("<p>").addClass("card-text").html("&ensp;"+ paramCourses.duration +"&ensp;" + paramCourses.level)))
                                    .append($("<p>").addClass("font-weight-bold").html("$"+paramCourses.discountPrice + "&emsp;")
                                        .append($("<del>").addClass("font-weight-light text-muted text-muted").html("$" + paramCourses.price))
                                )
                        )
                        .append($("<ul>").addClass("list-group list-group-flush")
                            .append($("<li>").addClass("list-group-item d-flex justify-content-between align-items-center")
                                .append($("<img>").addClass("avatar-img").attr("src",paramCourses.teacherPhoto))
                                .append($("<p>").addClass("mb-2").html("&ensp;" + paramCourses.teacherName ))
                                .append($("<i>").addClass("fa-regular fa-bookmark")))
                                )
                            )
                        )
                        
                }).join("");
            }
            // hàm tìm kiếm course trending
            function findCourseIsTrending(){
                "use strict";
                var vObject = [];
                for(var bI = 0; bI < gCoursesDB.courses.length; bI++){
                    if(gCoursesDB.courses[bI].isTrending === true){
                        vObject.push(gCoursesDB.courses[bI]);
                    }
                }
                return vObject;
            }