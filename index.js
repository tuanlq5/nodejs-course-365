//khai báo express js
const {response, request} = require("express");
const express = require("express");
//khai báo mongoose
const mongoose = require("mongoose");

//khởi tạo path
const path = require("path");

//khởi tạo app express
const app = express()

//khai báo cổng chạy app
const port = 8000;

//khai báo router
const courseRouter = require("./app/router/courseRouter");
// khai báo model
const courseModel = require("./app/models/courseModel");
//cấu hình request đọc 
app.use(express.json());
//load ảnh trong trang web
app.use(express.static(__dirname + "/views" ));
//khai báo API dạng GET "/" sẽ chạy vào file pizza 365
app.get("/",(request, response) => {
    response.sendFile(path.join(__dirname + "/views/CoursesPage.html"))
})
//kết nối với mongooseDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365", function(error){
    if(error)throw error;
    console.log(`MongoDB successfully connected`);
})
// app sử dụng router
app.use("/", courseRouter);
//listen when port is ready
app.listen(port, () =>{
    console.log(`App listening to port`, port);
})